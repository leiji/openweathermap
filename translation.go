package openweathermap

// translations for weather codes http://openweathermap.org/wiki/API/Weather_Condition_Codes

var ConditionDescriptions map[string](map[int]string) = map[string](map[int]string){
	"de": map[int]string{
		200: "Gewitter mit leichtem Regen",       // 	thunderstorm with light rain
		201: "Gewitter mit Regen",                //	thunderstorm with rain 	11d.png
		202: "Gewitter mit starkem Regen",        //	thunderstorm with heavy rain 	11d.png
		210: "Leichtes Gewitter",                 //	light thunderstorm 	11d.png
		211: "Gewitter",                          //	thunderstorm 	11d.png
		212: "Schweres Gewitter",                 //	heavy thunderstorm 	11d.png
		221: "Zerrissenes Gewitter",              //	ragged thunderstorm 	11d.png
		230: "Gewitter mit leichtem Nieselregen", //	thunderstorm with light drizzle 	11d.png
		231: "Gewitter mit Nieselregen",          //	thunderstorm with drizzle 	11d.png
		232: "Gewitter mit starkem Nieselregen",  //	thunderstorm with heavy drizzle 	11d.png
		300: "Leichter Nieselregen",              //	light intensity drizzle 	09d.png
		301: "Nieselregen",                       //	drizzle 	09d.png
		302: "Starker Nieselregen",               //	heavy intensity drizzle 	09d.png
		310: "Leichter Niesel/Regen",             //	light intensity drizzle rain 	09d.png
		311: "Niesel/Regen",                      //	drizzle rain 	09d.png
		312: "Starker Niesel/Regen",              //	heavy intensity drizzle rain 	09d.png
		321: "Niesel/Schauer",                    //	shower drizzle 	09d.png
		500: "Leichter Regen",                    //	light rain 	10d.png
		501: "Regen",                             //	moderate rain 	10d.png
		502: "Starker Regen",                     //	heavy intensity rain 	10d.png
		503: "Sehr starker Regen",                //	very heavy rain 	10d.png
		504: "Extremer Regen",                    //	extreme rain 	10d.png
		511: "Eisregen",                          // \m/,	freezing rain 	13d.png
		520: "Leichter Regenschauer",             //	light intensity shower rain 	09d.png
		521: "Regenschauer",                      //	shower rain 	09d.png
		522: "Starker Regenschauer",              //	heavy intensity shower rain
		600: "Leichter Schneefall",               //	light snow 	13d.png
		601: "Schnee",                            //	snow 	13d.png
		602: "Starker Schneefall",                //	heavy snow 	13d.png
		611: "Schneeregen",                       //	sleet 	13d.png
		621: "Schneeschauer",                     //	shower snow 	13d.png
		701: "Leichter Nebel",                    //	mist 	50d.png
		711: "Rauch",                             //	smoke 	50d.png
		721: "Dunst",                             //	haze 	50d.png
		731: "Sand-/Staubwirbel",                 //	Sand/Dust Whirls 	50d.png
		741: "Nebel",                             //	Fog
		800: "Klar",                              //	sky is clear 	01d.png 01n.png
		801: "Heiter",                            //	few clouds 	02d.png 02n.png
		802: "Bewölkt",                           //	scattered clouds 	03d.png 03d.png
		803: "Stark bewölkt",                     //	broken clouds 	04d.png 03d.png
		804: "Bedeckt",                           //	overcast clouds 	04d.png
		900: "Tornado",                           //	tornado
		901: "Tropischer Sturm",                  //	tropical storm
		902: "Hurricane",                         //	hurricane
		903: "Kalt",                              //	cold
		904: "Heiß",                              //	hot
		905: "Windig",                            //	windy
		906: "Hagel",                             //	hail
	},
}
