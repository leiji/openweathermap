// Small demo app for the openweathermap package
// +build ignore
package main

import "bitbucket.org/leiji/openweathermap"
import "fmt"
import "time"

func main() {

	dresden := openweathermap.City{
		Coord: openweathermap.Coord{
			Lat: 51.049259,
			Lng: 13.73836,
		},
		Name: "Dresden",
		Id:   2935022,
	}

	wd, err := dresden.Current()
	if err != nil {
		fmt.Printf("Error: %s\n", err)
	} else {
		fmt.Printf("Weather in %s: %s (min/max: %s/%s) %s\n", wd.Name, wd.Main.Temp, wd.Main.TempMin, wd.Main.TempMax, wd.Weather[0].L10n("de"))
	}

	fd, err := dresden.Forecast()
	if err != nil {
		fmt.Printf("Error: %s\n", err)
	} else {
		fmt.Printf("Weather forecast in %s\n", fd.City.Name)
		for _, v := range fd.List {
			t := time.Unix(int64(v.Dt), 0)
			fmt.Printf("%s: %s -- %s\n", t.Format("20060201-1504 Monday"), v.Main.Temp, v.Weather[0].L10n("de"))
		}
	}

}
