// OpenWeatherMap.org interface for go.
//
// Only small parts of the API are implemented for now, but the rest should be trivial and will be added when needed.
// You do need the City ID to query the current weather and the forecast.
// See the demo application for simple usage.
// 
// http://openweathermap.org/wiki/API/2.0/Weather_Data 
//
// http://openweathermap.org/wiki/API/Weather_Condition_Codes
//
package openweathermap

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
)

/*

{"id":88319,"dt":1345284000,"name":"Benghazi",
    "coord":{"lat":32.12,"lon":20.07},
    "main":{"temp":306.15,"pressure":1013,"humidity":44,"temp_min":306,"temp_max":306},
    "wind":{"speed":1,"deg":-7},
    "weather":[
                 {"id":520,"main":"Rain","description":"light intensity shower rain","icon":"09d"},
                 {"id":500,"main":"Rain","description":"light rain","icon":"10d"},
                 {"id":701,"main":"Mist","description":"mist","icon":"50d"}
              ],
    "clouds":{"all":90},
    "rain":{"3h":3}}
*/

// http://openweathermap.org/wiki/API/2.0/Weather_Data

const _USERAGENT = "openweathermap go package"

type WeatherData struct {
	Name    string  `json:"name"`
	Id      int     `json:"id"`
	Coord   Coord   `json:"coord"`
	Dt      int     `json:"dt"`
	Main    Main    `json:"main"`
	Wind    Wind    `json:"wind"`
	Clouds  Clouds  `json:"clouds",omitempty`
	Rain    Rain    `json:"rain",omitempty`
	Weather Weather `json:"weather"`
}

type Coord struct {
	Lat float64
	Lng float64
}

type Main struct {
	Temp     Temp    `json:"temp"`
	Pressure float64 `json:"pressure"`
	Humidity float64 `json:"humidity"`
	TempMin  Temp    `json:"temp_min"`
	TempMax  Temp    `json:"temp_max"`
}

// Temperature, stored in Kelvin
type Temp float64

// As °C
func (t Temp) C() float64 {
	return float64(t) - 273.15
}

// Pretty print as "%0.1f°C" (12.3°C)
func (t Temp) String() string {
	return fmt.Sprintf("%0.1f°C", t.C())
}

// As Kelvin
func (t Temp) K() float64 {
	return float64(t)
}

type Wind struct {
	Speed float64 `json:"speed"`
	Deg   float64 `json:"deg"`
}

type Weather []WeatherInfo // `json:"weather"`

type WeatherInfo struct {
	Id          int    `json:"id"`
	Main        string `json:"main"`
	Description string `json:"description"`
	Icon        string `json:"icon"`
}

// Translate the description to another language.
// Right now: "de" works. If the string cannot be resolved, the default description is returned.
func (wi WeatherInfo) L10n(lang string) string {
	if _, ok := ConditionDescriptions[lang]; ok {
		if _, oktoo := ConditionDescriptions[lang][wi.Id]; oktoo {
			return ConditionDescriptions[lang][wi.Id]
		}
	}
	return wi.Description
}

type Clouds map[string]float64
type Rain map[string]float64

// http://openweathermap.org/wiki/API/Weather_Condition_Codes

/* {{{

{"message":"","cod":"200","type":"geonames_lang-name","calctime":0.0485,
"list":[

{"id":5128638,
"coord":{"lat":43.000351,"lon":-75.499901},
"name":"New York",
"main":{
    "temp":285,"temp_min":284.8,"temp_max":289.8,"pressure":999.16,"humidity":98.9},"dt":1347646686,"date":"2012-09-14 19:18:06","wind":{"speed":2.58,"deg":276,"gust":7.4},
    "rain":{"3h":4.8},
    "clouds":{"all":82},
    "weather":[{"id":501,"main":"Rain","description":"moderate rain","icon":"10d"}],
    "url":"http:\/\/openweathermap.org\/city\/5128638"
}]}

}}} */

/*
http://api.openweathermap.org/data/2.1/weather/city/2935022 {{{

{ id: 2935022,
  coord: { lat: 51.050892, lon: 13.73832 },
  name: 'Dresden',
  main: 
   { temp: 268.02,
     pressure: 1021,
     humidity: 41,
     temp_min: 263.71,
     temp_max: 269.15 },
  dt: 1364120400,
  date: '2013-03-24 10:20:00',
  wind: { speed: 10.3, deg: 90 },
  clouds: { all: 0 },
  weather: 
   [ { id: 800,
       main: 'Clear',
       description: 'Sky is Clear',
       icon: '01d' } ],
  sys: { id: 4879, country: 'DE' },
  url: 'http://openweathermap.org/city/2935022',
  cod: 200 }

}}} */

// City to query. Id is the only relevant field. Get the id here: http://openweathermap.org/Maps
type City struct {
	Id         int    `json:"id"`
	Name       string `json:"name"`
	Coord      Coord  `json:"coord"`
	Country    string `json:"country",omitempty`
	Population int    `json:"population",omitempty`
}

// Get the current weather
func (c City) Current() (*WeatherData, error) {
	client := &http.Client{}
	uri := fmt.Sprintf("http://api.openweathermap.org/data/2.1/weather/city/%d", c.Id)
	//fmt.Printf("URI %s\n", uri)
	req, err := http.NewRequest("GET", uri, nil)
	if err != nil {
		return nil, err
	}
	req.Header.Set("User-Agent", _USERAGENT)
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	//fmt.Printf("RESP %+v\n", resp)
	defer resp.Body.Close()

	data, err := ioutil.ReadAll(resp.Body)
	databuf := bytes.NewBuffer(data)
	//	fmt.Printf("DATA %d: %s\n",len(data), databuf)
	wd := new(WeatherData)
	dec := json.NewDecoder(databuf)
	err = dec.Decode(wd)
	if err != nil && err != io.EOF {
		return nil, err
	}

	return wd, nil

}

// forecasts for city:
// http://api.openweathermap.org/data/2.1/forecast/city/2935022
/* {{{

{ cod: '200',
  city: 
   { id: 2935022,
     name: 'Dresden',
     coord: { lon: 13.73832, lat: 51.050892 },
     country: 'DE',
     population: 486854 },
  cnt: 61,
  model: 'OWM',
  list: 
   [ { dt: 1364122800,
       main: [Object],
       weather: [Object],
       clouds: [Object],
       wind: [Object],
       dt_txt: '2013-03-24 11:00:00' },
....... snip ....
     { dt: 1364756400,
       main: [Object],
       weather: [Object],
       clouds: [Object],
       wind: [Object],
       dt_txt: '2013-03-31 19:00:00' },
     { dt: 1364767200,
       main: [Object],
       weather: [Object],
       clouds: [Object],
       wind: [Object],
       dt_txt: '2013-03-31 22:00:00' } ] }

		 }}}
*/

// Forecast response
type ForecastData struct {
	Cod  string `json:"cod"`
	City City   `json:"city"`
	List []WeatherData
}

// Get the forecast data
func (c *City) Forecast() (*ForecastData, error) {
	client := &http.Client{}
	uri := fmt.Sprintf("http://api.openweathermap.org/data/2.1/forecast/city/%d", c.Id)
	//fmt.Printf("URI %s\n", uri)
	req, err := http.NewRequest("GET", uri, nil)
	if err != nil {
		return nil, err
	}
	req.Header.Set("User-Agent", _USERAGENT)
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	//fmt.Printf("RESP %+v\n", resp)
	defer resp.Body.Close()

	data, err := ioutil.ReadAll(resp.Body)
	databuf := bytes.NewBuffer(data)
	//fmt.Printf("DATA %d: %s\n", len(data), databuf)
	fd := new(ForecastData)
	dec := json.NewDecoder(databuf)
	err = dec.Decode(fd)
	if err != nil && err != io.EOF {
		return nil, err
	}

	return fd, nil
}
